export default {
    entry: 'dist-npm/index.js',
    dest: 'dist-npm/bundles/test-pv.umd.js',
    sourceMap: false,
    format: 'umd',
    moduleName: 'ng.test-pv'
  }
