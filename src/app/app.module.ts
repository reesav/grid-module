import { NgModule, Injector } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { createCustomElement } from '@angular/elements';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from './material.module';

import { AgGridModule } from 'ag-grid-angular/main';

import { RowButtonComponent } from './productview/row-button.components';
import { TableComponent } from './productview/table/table.component';

import { PipesModule } from './pipes/pipes.module';


@NgModule({
  entryComponents: [TableComponent],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    AgGridModule.withComponents([
        RowButtonComponent,
    ]),
    MaterialModule,
    PipesModule
  ],
  declarations: [
    RowButtonComponent,
    TableComponent
  ],
})
export class ProductViewModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap() {
    const prodView = createCustomElement(TableComponent, { injector: this.injector });
    customElements.define('product-view', prodView);
  }
}
