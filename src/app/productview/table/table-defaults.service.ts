import { Injectable } from "@angular/core";

import * as _ from "lodash";
import * as moment from "moment";

@Injectable()
export class TableDefaultsService {
    private tableFilterIcon =
        "<div class='column-filter-header-parent'> <span class='ag-icon ag-filter-icon'></span> <span>Refine Search</span> </div>";
    private tableDefDefaults = {
        toolPanelSuppressRowGroups: true,
        toolPanelSuppressValues: true,
        toolPanelSuppressPivots: true,
        toolPanelSuppressPivotMode: true,
        toolPanelSuppressSideButtons: true,
        enableRangeSelection: true,
        enableColResize: true,
        enableSorting: true,
        enableFilter: true,
        floatingFilter: true,
        rowSelection: "multiple",
        rowHeight: 45,
        suppressPropertyNamesCheck: true,
        headerHeight: 32,
        icons: {
            filter: this.tableFilterIcon
        },
        postProcessPopup: this.repositionFilterMenu
    };

    private colDefDefaults = {
        filter: "agTextColumnFilter",
        suppressFilter: false,
        suppressMenu: true
    };

    private actionDefaults = {
        cellClass: "table-btn",
        pinned: "right",
        suppressToolPanel: true,
        suppressMenu: true
    };

    public getDefaults(defaultType: string): any {
        if (defaultType === "tableDefDefaults") {
            return _.cloneDeep(this.tableDefDefaults);
        }
        if (defaultType === "colDefDefaults") {
            return _.cloneDeep(this.colDefDefaults);
        }
        if (defaultType === "actionDefaults") {
            return _.cloneDeep(this.actionDefaults);
        }
        if (defaultType === "contextMenu") {
            return this.getContextMenuItems;
        }
    }

    public dateComparator(filterDate: Date, cellValue: string): number {
        // Cell value date is ISO 8601 (ish)
        // Remove time information from cell value as filter input is a date
        const cellDate = moment(cellValue.split(" ")[0]);

        // Now that both parameters are Date objects, we can compare
        if (cellDate.isBefore(filterDate)) {
            return -1;
        } else if (cellDate.isAfter(filterDate)) {
            return 1;
        } else {
            return 0;
        }
    }

    public repositionFilterMenu(params) {
        if (params.type !== "columnMenu") {
            return;
        }
        const columnId = params.column.getId();
        const internalColDefs =
            params.column.gridOptionsWrapper.gridOptions.columnDefs;
        const lastColFilterType =
            internalColDefs[internalColDefs.length - 2].filter;
        if (lastColFilterType === "set" || lastColFilterType === "date") {
            const ePopup = params.ePopup;

            let oldLeftStr = ePopup.style.left;
            // remove 'px' from the string (ag-Grid uses px positioning)
            oldLeftStr = oldLeftStr.substring(0, oldLeftStr.indexOf("px"));
            const oldLeft = parseInt(oldLeftStr);
            let newLeft;
            // HACK -y as shit
            columnId === "manifestation" || columnId === "status"
                ? (newLeft = oldLeft - 16)
                : (newLeft = oldLeft - 50);
            ePopup.style.left = newLeft + "px";
        }
    }

    public updateNullValues(params): void {
        return params.value === null || params.value === ""
            ? "(BLANK)"
            : params.value;
    }

    public getContextMenuItems(params) {
        const contextMenuResults = [
            {
                name: "Copy Single Cell",
                action: () => {
                    params.api.copySelectedRangeToClipboard();
                },
                icon: '<i class="material-icons">content_copy</i>'
            },
            {
                name: "Copy Selected Row(s)",
                action: () => {
                    params.context.tableTools.copySelectedRows(false);
                },
                icon: '<i class="material-icons">content_copy</i>'
            },
            {
                name: "Copy Selected Row(s) With Headers",
                action: () => {
                    params.context.tableTools.copySelectedRows(true);
                },
                icon: '<i class="material-icons">content_copy</i>'
            },
            "separator",
            {
                name: params.api.isToolPanelShowing()
                    ? "Hide Tool Panel"
                    : "Show Tool Panel",
                action: () => {
                    params.context.tableTools.enhanceTableSearch();
                },
                icon: '<i class="material-icons">build</i>'
            },
            {
                name: "Export Table",
                icon: '<i class="material-icons">file_download</i>',
                subMenu: [
                    {
                        name: "Export as XLS",
                        action: () => {
                            params.context.tableTools.exportToExcel();
                        },
                        icon: '<i class="material-icons">file_download</i>'
                    },
                    {
                        name: "Export as CSV",
                        action: () => {
                            params.context.tableTools.exportAsCSV();
                        },
                        icon: '<i class="material-icons">file_download</i>'
                    }
                ]
            }
        ];
        return contextMenuResults;
    }
}
