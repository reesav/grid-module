
import {distinctUntilChanged} from 'rxjs/operators';
import {
    Component,
    EventEmitter,
    OnDestroy,
    OnInit,
    Output,
    Input,
    ViewChild,
    ViewEncapsulation
} from '@angular/core';

import { GridOptions } from 'ag-grid/main';

import { Subject } from 'rxjs';
import 'rxjs';

import * as moment from 'moment';
import * as _ from 'lodash';

import { CustomGridOptions, FilterableColumn } from './table-types';

import { TableDefaultsService } from './table-defaults.service';
import { RowButtonEvent, RowButtonComponent } from '../row-button.components';
// import { TableToolsComponent } from './table-tools/table-tools.component';
import { CellRendererFactory } from './cell-renderer.factory';

export type TableOptions = GridOptions & CustomGridOptions;

// // TODO: tighten up
export type Actions = any;
export type TableDef = any;
export type Data = any[];

@Component({
    selector: 'cmn-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.scss'],

    providers: [CellRendererFactory, TableDefaultsService],
    encapsulation: ViewEncapsulation.Emulated
})
export class TableComponent implements OnInit, OnDestroy {
    public columnDefs: any[];
    public columnNames: any[] = [];
    public filterableColumns: FilterableColumn[] = [];
    private filterTerms = new Subject<string>();
    public gridOptions: TableOptions = {};
    public toolPanelVisibility: string = 'hidden';
    public overlayLoadingTemplate = '<span class="ag-overlay-loading-center" >Please wait while your rows are loading</span>';
    public getContextMenuItems;

    private _data: Data = null;
    public _tableDef: TableDef;

    @Input() rowClassFn?: (params: any) => any;
    @Input() rowStyleFn?: (params: any) => any;
    @Input() exportPrefix?: string;
    @Input() sortOnLoad?: string;
    @Input() tableHeader?: string;
    @Input() extraTableToolBtn?: string;

    // boolean for tree format tables
    @Input() groupDefaultExpanded?: number;
    @Input() animateRows?: boolean;
    @Input() treeData?: boolean;
    @Input() autoGroupColumnDef?: any;
    @Input() getDataPath?: (params: any) => any;

    @Input() exportSearchParams;
    @Output('search-terms')
    searchTerms: EventEmitter<any> = new EventEmitter<any>();

    @Input()
    set updateFilter(search) {
        if (search && Object.keys(search).length) {
            this.gridApi.setFilterModel(search);
        } else if (search === {}) {
            this.gridApi.setFilterModel(null);
        }
    }

    @Input() exportRowWhenClicked;
    @Output('clicked-row')
    exportClickedRow: EventEmitter<any> = new EventEmitter<any>();

    @Input()
    set data(theData: Data) {
        if (!theData && this.gridApi) {
            this.gridApi.showLoadingOverlay();
        }
        if (!theData) {
            return;
        }
        this._data = theData;

        if (!this.gridApi) {
            return;
        }

        this.gridApi.setRowData(this._data);
    }

    @Input()
    set tableDefinition(tD: TableDef) {
        if (!tD) {
            return;
        }
        this._tableDef = _.cloneDeep(tD);

        this.gridOptions = this.createGridOptions();

        this.columnDefs = this.createColumnDefs();

        const actions = this.manageTableActions();
        if (actions) {
            this.columnDefs.push(actions);
        }
    }

    @Output() extraToolBtnUsed: EventEmitter<any> = new EventEmitter<any>();

    @Output('row-button-click')
    buttonOutput: EventEmitter<RowButtonEvent> = new EventEmitter<
        RowButtonEvent
        >();

    // @ViewChild(TableToolsComponent) tableTools: TableToolsComponent;

    constructor(
        private cellFactory: CellRendererFactory,
        public defaults: TableDefaultsService,
    ) { }

    ngOnInit() {
        // returns custom context menu
        this.getContextMenuItems = this.defaults.getDefaults('contextMenu');
        this.filterTerms.pipe(distinctUntilChanged()).subscribe(filterTerm => {
            // Always set the filter string if it changes

            if (!this.gridApi) {
                return;
            }

            this.gridApi.setRowData(this._data);
            this.gridApi.onFilterChanged();
        });
    }

    public gridApi;
    public gridColumnApi;
    public gridExportPrefix;
    apiReady() {
        this.gridApi = this.gridOptions.api;
        this.gridColumnApi = this.gridOptions.columnApi;
        this.gridExportPrefix = this.gridOptions.exportPrefix;

        this.gridApi.onFilterChanged;
        if (!this._data) {
            this.gridApi.showLoadingOverlay();
        } else {
            this.gridApi.setRowData(this._data);
        }
        if (this.sortOnLoad) {
            const sortModel = [{ colId: this.sortOnLoad, sort: 'desc' }];
            this.gridApi.setSortModel(sortModel);
        }
    }

    public onFilterUpdate() {
        if (!this.exportSearchParams) {
            return;
        } else {
            const filteredSearchResults = [];
            this.gridApi.forEachNodeAfterFilter(rowNode => {
                filteredSearchResults[filteredSearchResults.length] =
                    rowNode.data;
            });
            const filterTerms = this.gridApi.getFilterModel();
            const results = {
                searchTerms: filterTerms,
                searchResults: filteredSearchResults,
            };
            this.searchTerms.emit(results);
        }
    }

    public onRowClicked(row) {
        if (!this.exportRowWhenClicked) {
            return;
        } else {
            this.exportClickedRow.emit(row.data);
        }
    }

    private createColumnDefs(): any {
        const table = this._tableDef;

        const columnDefs = [];
        for (const col of table.columnDefs) {
            // Set up fuzzy filter columns
            if (col.headerName) {
                this.filterableColumns.push(<FilterableColumn>{
                    name: col.headerName,
                    field: col.field,
                    includeInSearch: col.includeInSearch,
                });
            }

            // For exporting
            if (col.field) {
                this.columnNames.push(col.field);
            }

            // TODO: checkboxSelection filters
            if (!col.filter && !col.checkboxSelection) {
                col.filter = 'agTextColumnFilter';
            }
            if (col.checkboxSelection === true) {
                col.suppressCopy = true;
            }

            if (col.filter === 'set') {
                col.filterParams = {
                    cellRenderer: this.defaults.updateNullValues,
                };
            }
            if (col.filter === 'date') {
                col.filterParams = {
                    comparator: this.defaults.dateComparator,
                };
            }
            if (!col.floatingFilterComponentParams) {
                col.floatingFilterComponentParams = {
                    suppressFilterButton: true,
                };
            }

            // Cell Renderers
            this.cellFactory.setRenderer(col);

            columnDefs.push({
                ...this.defaults.getDefaults('colDefDefaults'),
                ...col,
            });
        }
        return columnDefs;
    }

    private createGridOptions(): TableOptions {
        return <TableOptions>{
            ...this.defaults.getDefaults('tableDefDefaults'),
            ...this._tableDef.gridOptions,
            context: this,
            getRowStyle: this.rowStyleFn,
            getRowClass: this.rowClassFn
        };
    }

    private manageTableActions(): any {
        let actionCol = this._tableDef.actions;

        if (!actionCol) {
            return;
        }

        if (!actionCol.width) {
            // buttons are 45px wide, with 10px of padding inbetween
            actionCol.width =
                45 * actionCol.buttons.length +
                10 * actionCol.buttons.length -
                1;
        }

        this.cellFactory.setRenderer(actionCol);

        actionCol = {
            ...this.defaults.getDefaults('actionDefaults'),
            ...actionCol,
        };

        return actionCol;
    }

    public updateColsToFilter(
        column: FilterableColumn,
        event: KeyboardEvent
    ): void {
        event.stopPropagation();
        column.includeInSearch = !column.includeInSearch;

        this.gridOptions.api.setRowData(this._data);
        this.gridOptions.api.onFilterChanged();
    }

    public filter(term: string): void {
        this.filterTerms.next(term);
    }

    public resizeGrid(calledFromWhere: string): void {
        if (this.gridOptions.api) {
            this.gridOptions.api.sizeColumnsToFit();
        } else if (this.gridApi) {
            this.gridApi.sizeColumnsToFit();
        }
    }

    public extraToolUsed(event) {
        this.extraToolBtnUsed.emit(event);
    }

    ngOnDestroy() {
        // TODO: what to clean up?
        // this.gridOptions = null;
    }
}
