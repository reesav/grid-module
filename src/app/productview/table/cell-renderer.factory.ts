import { Injectable } from "@angular/core";

import { RowButtonComponent } from "../row-button.components";

@Injectable()
export class CellRendererFactory {
    public setRenderer(column: any): void {
        if (column.buttons) {
            column.cellRendererFramework = RowButtonComponent;
        }
    }
}
