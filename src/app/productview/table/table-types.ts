export type CustomGridOptions = {
    exportPrefix?: string;
};

export class FilterableColumn {
    name: string;
    field: string;
    includeInSearch: boolean;
}
