import { Component, EventEmitter } from '@angular/core';
import { AgRendererComponent } from 'ag-grid-angular/main';
import { ActivatedRoute, Router } from '@angular/router';

export type Button = {
    type: string;
    icon: string;
    color: string;
    navigateTo?: string | string[];
    queryParams?: { [key: string]: string };
    newWindow?: boolean;
};

export type RowButtonEvent = { type: string; rowData: any };

@Component({
    selector: "ag-row-button",
    template: `
    <button mat-icon-button
            [style.color]="button.color"
            (click)="clicked(button)"
            *ngFor="let button of buttons">
      <span class="material-icons" [ngClass]="button.icon">
        <mat-icon>{{button.icon}}</mat-icon>
      </span>
    </button>
    `
})
export class RowButtonComponent implements AgRendererComponent {
    private data: any;
    private tableData: any;
    public buttons: Button[];
    private idOfMostRecentCommit = '';
    private outputEmitter: EventEmitter<RowButtonEvent>;

    constructor(
        public activatedRoute: ActivatedRoute,
        private router: Router
    ) { }

    agInit(params: any): void {
        // Row data
        this.data = params.data;
        this.tableData = params.context._data;

        this.buttons = params.colDef.buttons as Button[];
        this.setIcons();

        this.outputEmitter = params.context.buttonOutput;
    }

    private setIcons(): void {
        for (const button of this.buttons) {
            switch (button.type) {
                // Dual table, add row to selection
                case 'add': {
                    button.icon = 'add_circle';
                    button.color = '#03A9F4';
                    break;
                }
                case 'delete': {
                    // delete is set to remove the most recent breadcrumb and is used
                    // to route user after use - use remove if this is undesirable
                    button.icon = 'delete';
                    button.color = '#ef5350';
                    break;
                }
                case 'edit': {
                    button.icon = 'mode_edit';
                    button.color = '#4caf50';
                    break;
                }
                case 'info': {
                    button.icon = 'info_outline';
                    button.color = '#03A9F4';
                    break;
                }
                case 'topo-info': {
                    button.icon = 'info_outline';
                    button.color = '#03A9F4';
                    break;
                }
                case 'diff': {
                    button.icon = 'change_history';
                    button.color = '#03A9F4';
                    break;
                }
                // Dual table, remove row from selection
                case 'remove': {
                    button.icon = 'remove_circle';
                    button.color = '#F44336';
                    break;
                }
            }
        }
    }

    public clicked(button: Button): void {
        if (button.navigateTo) {
            // interpolate URL
            let url: string[] = [];
            if (typeof button.navigateTo === 'string') {
                url = [this.interpolate(button.navigateTo)];
            } else if (button.navigateTo instanceof Array) {
                for (const s of button.navigateTo) {
                    url.push(this.interpolate(s));
                }
            }

            const d: RowButtonEvent = {
                type: button.type,
                rowData: this.data
            };
            this.outputEmitter.emit(d);
        }
    }

    private interpolate(target: string): string {
        const interpolation = /\$\{(\w+)\}/g;
        let match;
        while ((match = interpolation.exec(target))) {
            const key = match[1];
            // this checks for routing from within the tree table
            if (Array.isArray(this.data[key]) && this.data[key].length > 1) {
                target = target.replace('${' + key + '}', this.data[key][0]);
            } else {
                target = target.replace('${' + key + '}', this.data[key]);
            }
        }

        return target;
    }

    refresh(): boolean {
        return false;
    }
}